# HamLearning

Snippets (in italian) to learn something about ham radio, along the items of
["Programma di Esame"](https://www.mise.gov.it/images/stories/recuperi/Comunicazioni/Sub_Allegato_D_All.26.pdf) per il conseguimento della Patente di Radioamatore.

1. [ELETTRICITÀ, ELETTROMAGNETISMO E RADIOTECNICA - TEORIA](1.ELETTRICITÀ%2C_ELETTROMAGNETISMO_E_RADIOTECNICA_-_TEORIA.md)

2. [COMPONENTI](2.COMPONENTI.md)

3. [CIRCUITI](3.CIRCUITI.md)

4. [RICEVITORI](4.RICEVITORI.md)

5. [TRASMETTITORI](5.TRASMETTITORI.md)

6. [ANTENNE E LINEE DI TRASMISSIONE](6.ANTENNE_E_LINEE_DI_TRASMISSIONE.md)

7. [PROPAGAZIONE](7.PROPAGAZIONE.md)

8. [MISURE](8.MISURE.md)

9. [DISTURBI E PROTEZIONE](9.DISTURBI_E_PROTEZIONE.md)

10. [PROTEZIONE ELETTRICA](10.PROTEZIONE_ELETTRICA.md)
